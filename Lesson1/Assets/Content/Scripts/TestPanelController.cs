﻿using UnityEngine;
using System;
using UnityEngine.UI;
using System.Collections.Generic;

public class TestPanelController : MonoBehaviour
{
	[SerializeField]
	private Text _textField;
	private ActionEnum _actionEnum;
	private int _firstNumber;
	private int _secondNumber;
	private float _result;

	public void GetNumbers(int index)
	{
		_textField.text += index;
	}

	public void GetActions(int actionId)
	{
		switch((ActionEnum)actionId)
		{
			case ActionEnum.Minus:
				SaveFirstNumber();
				_actionEnum = ActionEnum.Minus;
				break;
			case ActionEnum.Plus:
				SaveFirstNumber();
				_actionEnum = ActionEnum.Plus;
				break;
			case ActionEnum.Ravno:
				EquelLogic();
				break;
			case ActionEnum.Razdelit:
				SaveFirstNumber();
				_actionEnum = ActionEnum.Razdelit;
				break;
			case ActionEnum.Reset:
				_textField.text = "";
				_firstNumber = 0;
				_secondNumber = 0;
				_actionEnum = ActionEnum.Default;
				_result = 0.0f;
				break;
			case ActionEnum.Umnojit:
				SaveFirstNumber();
				_actionEnum = ActionEnum.Umnojit;
				break;
		}
	}

	private void EquelLogic()
	{
		if(_textField.text != "")
		{
			_secondNumber = Convert.ToInt32(_textField.text);
		}

		switch(_actionEnum)
		{
			case ActionEnum.Minus:
				_result = _firstNumber - _secondNumber;
				break;
			case ActionEnum.Plus:
				_result = _firstNumber + _secondNumber;
				break;
			case ActionEnum.Umnojit:
				_result = _firstNumber * _secondNumber;
				break;
			case ActionEnum.Razdelit:
				_result = _firstNumber / _secondNumber;
				break;
		}

		_textField.text = _result.ToString();
	}

	private void SaveFirstNumber()
	{
		if(_textField.text != "")
		{
			_firstNumber = Convert.ToInt32(_textField.text);
		}

		_textField.text = "0";
	}
} 

public enum ActionEnum
{
	Umnojit = 0,
	Razdelit = 1,
	Plus = 2,
	Minus = 3,
	Ravno = 4,
	Reset = 5,
	Default = 6
}