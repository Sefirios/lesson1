﻿using UnityEngine;
using System;

public class TestClass : MonoBehaviour
{
	#region hjgjjg

	[SerializeField]
	private int _testInt;// целочисленная переменная
	private float _testFloat;//переменная с плавающей точкой
	private string _testString;//строковая переменная
	[SerializeField]
	private bool _testBool;//Логическая переменная
	private Calc _calc;

	#endregion

	private void Start() // метод который отрабатывает сцену и не возвращает
	{
		_calc = new Calc();
		Debug.Log(_calc.MyMethod(10, -10));
		Debug.Log(_calc.MyMethod(15, -10));
		Debug.Log(_calc.MyMethod(0, -10));

		if(!this.gameObject.isStatic)
		{
			Debug.Log("Dynamic");
		}
		else
		{
			Debug.Log("Static");
		}
	}

	private	void Update()
	{
		if(Convert.ToInt32(Time.time) !=_testInt)
		{
			_testInt = Convert.ToInt32(Time.time);
		
			if(_testInt % 5 == 0)
			{
				_testBool = !_testBool;
				Debug.Log(string.Format("Time= {0} testBool is {1}", Time.time, _testBool));
			}
		}
	}
}
