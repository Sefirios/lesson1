﻿public class Calc
{
	public int MyMethod(int index,
	                     int index2)
	{
		if(index < 0 || index2 > 0)
		{
			index--;
		}
		else if(index > 0)
		{
			index++;
		}
		else
		{
			index += 17;
		}

		return index;
	}
}