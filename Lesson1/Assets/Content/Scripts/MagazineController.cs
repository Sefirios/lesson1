﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;
using UnityEngine;

public class MagazineController : MonoBehaviour 
{
  [SerializeField]
	private Text _textfield;
	[SerializeField]
	private Text[] _textName;
	[SerializeField]
	private Dropdown[] _dropdown;
	[SerializeField]
	private Text[] _priceText;
	private List<float[]> _priceValue; 

	private void Start ()
	{
		InitValue();
		Calc();
	}

	private void InitValue()
	{
		#region PriceValue
		_priceValue = new List<float[]>();
		_priceValue.Add(new float[6] {0, 50f, 80f, 140f, 220f, 400.46f });
		_priceValue.Add(new float[4] {0, 200f,450f,650.22f});
		_priceValue.Add(new float[5] {0, 65f,130f,170f,240.22f});
		_priceValue.Add(new float[4] {0, 14f,250,400.46f});
		_priceValue.Add(new float[7] {0, 17f,46f,137f,200f,450f,650.22f});
		_priceValue.Add(new float[4] {0, 118f,195f,237.25f});
		#endregion

		#region Dropdown
		_dropdown[0].AddOptions(new List<string>() {"Please Select Item", "Asus", "Relatek","HP", "Gigabyte", "Dell" });
		_dropdown[1].AddOptions(new List<string>() {"Please Select Item", "I5", "I3", "I7",});	
		_dropdown[2].AddOptions(new List<string>() {"Please Select Item", "V1", "V2", "V3", "V4" });
		_dropdown[3].AddOptions(new List<string>() {"Please Select Item", "2G", "4G", "16G" });
		_dropdown[4].AddOptions(new List<string>() {"Please Select Item", "200W", "250W", "300W", "350W", "400W", "450W" });
		_dropdown[5].AddOptions(new List<string>() {"Please Select Item", "500GB", "1TB", "2TB" });
		#endregion

		#region TextName
		_textName[0].text = "Motherboard Type";
		_textName[1].text = "Processor Type";
		_textName[2].text = "Video Card Type";
		_textName[3].text = "Operative Memory Type";
		_textName[4].text = "Power Block Type";
		_textName[5].text = "Hard Disk Type";
		#endregion
	}
	private void Calc()
	{
		float finalPrice = 0;
		Color textColor;
		textColor = Color.white;

		for(int index = 0; index < _textName.Length; index++)
		{
			finalPrice += _priceValue[index][_dropdown[index].value];
			Debug.Log(string.Format("i={0}; Price={1}", index ,_priceValue[index][1]));

		}

		if(finalPrice < 500)
		{
			textColor = Color.green;
		}
		else if(finalPrice > 500 && finalPrice < 1000)
			{
				textColor = Color.yellow;
			}
			else if(finalPrice > 1000)
				{
					textColor = Color.red;
				}
		_textfield.color = textColor;
		_textfield.text = finalPrice.ToString();
	}

	public void ChangeValue(int index)
	{
		_priceText[index].text = _priceValue[index][_dropdown[index].value].ToString();
		Calc();
	}
}


