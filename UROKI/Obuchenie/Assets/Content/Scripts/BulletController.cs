﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController: MonoBehaviour 
{
	[SerializeField]
	private float _bulletSpeed;
	[SerializeField]
	private float _bulletDestroyDistance;
	private GameObject _pawn;

	public void InitBullet(GameObject pawn)
	{
		_pawn = pawn;
		StartCoroutine("LoopCoroutine");
	}

	private IEnumerator LoopCoroutine()
	{
		while(true)
		{
			transform.Translate(Vector3.forward * _bulletSpeed * Time.deltaTime);

			if(Vector3.Distance(_pawn.transform.position, transform.position) >= _bulletDestroyDistance)
			{
				DestroyBullet();
			}

			yield return null;
		}
	}

	private void DestroyBullet()
	{
		StopCoroutine("LoopCoroutine");
		_pawn.GetComponent<PawnController>().PullBullets.Add(gameObject);
		gameObject.SetActive(false);
	}
}
