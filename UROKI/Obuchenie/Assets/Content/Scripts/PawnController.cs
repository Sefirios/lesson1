﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PawnController: MonoBehaviour 
{
	[SerializeField]
	private GameObject _bulletPref;
	[SerializeField]
	private GameObject _spawnPoint;
	[SerializeField]
	private List<GameObject> _pullBullets;
	public List<GameObject> PullBullets
	{
		get{ return _pullBullets; }
	}

	private void Start()
	{
		if(_bulletPref != null && _spawnPoint != null)
		{
			_pullBullets = new List<GameObject>();
			StartCoroutine("LoopCoroutine");
		}
	}

	private IEnumerator LoopCoroutine()
	{
		while(true)
		{
			if(Input.GetMouseButtonUp(0))
			{
				InstantiateBullet();
			}

			yield return null;
		}
	}

	private void InstantiateBullet()
	{
		if(_pullBullets.Count == 0)
		{
			GameObject newBullet = Instantiate(_bulletPref);
		
			AddBullet(newBullet);

		//	newBullet.transform.position = _spawnPoint.transform.position;
		//	newBullet.GetComponent<BulletController>().InitBullet(gameObject);
		}
		else
		{
			_pullBullets[0].SetActive(true);

			AddBullet(_pullBullets[0]);
		//	_pullBullets[0].transform.position = _spawnPoint.transform.position;
		//	_pullBullets[0].GetComponent<BulletController>().InitBullet(gameObject);


			_pullBullets.RemoveAt(0);
		}
	}

	private void AddBullet(GameObject addBullet)
	{
		addBullet.transform.position = _spawnPoint.transform.position;
		addBullet.GetComponent<BulletController>().InitBullet(gameObject);
	}
}