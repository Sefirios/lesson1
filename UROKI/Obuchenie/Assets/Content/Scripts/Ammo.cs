﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ammo : MonoBehaviour 
{
	private float _ammoSpeed = 10f;
	private float _ammoLife = 2.0f;

	IEnumerator Start()
	{

		yield return new WaitForSeconds(_ammoLife);
		Destroy(gameObject);
			
	}

	void OnCollisionEnter(Collision collision)
	{
		if(collision.gameObject.tag != "Player")
		{
			Destroy(gameObject);
		}
	}


	// Update is called once per frame
	void Update () 
	{
		transform.Translate(-Vector3.forward*_ammoSpeed*Time.deltaTime, Space.Self);
	}
}
