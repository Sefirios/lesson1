﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathZoneController : MonoBehaviour 
{

	void OnTriggerEnter2D(Collider2D col)
	{
		Debug.Log(string.Format("{0}", col.gameObject.name)); 
		PawnControll pawnControll = col.GetComponent<PawnControll>();
		pawnControll.Root.DestroyPawn(col.gameObject);
		Destroy(col.gameObject);
	
	}

}
