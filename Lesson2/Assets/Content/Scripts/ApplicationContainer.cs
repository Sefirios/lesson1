﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplicationContainer : MonoBehaviour 
{
	[SerializeField]
	private GameObject _prizrakPrefab;
	[SerializeField]
	private float _respawnTime;
	private bool _error = false;
	private float _timeShift;
	[SerializeField]
	private List<GameObject> _addedPrefabs = new List<GameObject>();
	private Vector3 _mousePosition;

	public List<GameObject> AddedPrefabs
	{
		get { return _addedPrefabs; }
	}

	void Start () 
	{
		_coll = GetComponent<Collider>();
		InstantiatePrizrak();
	}
	

	void Update () 
	{
		MousePositionCoordinates();
		if(!_error)
		{
			RespawnLogic();	
		}	
	}

	private void InstantiatePrizrak()
	{
		_timeShift = _respawnTime;
	
		if(_prizrakPrefab != null)
		{
			_addedPrefabs.Add(Instantiate(_prizrakPrefab));
			//Instantiate(_prizrakPrefab);
			PawnControll pawnControll = _addedPrefabs[_addedPrefabs.Count-1].GetComponent<PawnControll>();
			pawnControll.Root = this;
		}
		else
		{
			_error = true;
			Debug.LogError(string.Format("[{0}][InstantiatePrizrak]_prizrakPrefab is null!", GetType().Name));
		}
	}

	private void RespawnLogic()
	{
		_timeShift -= Time.deltaTime;
		if(_timeShift <= 0)
		{
			InstantiatePrizrak();
			_timeShift = _respawnTime;
		}
	}
	public void DestroyPawn(GameObject pawn)
	{
		_addedPrefabs.RemoveAt(_addedPrefabs.IndexOf(pawn));
	}
 

	private Collider _coll;

	private void MousePositionCoordinates()
	{
		if(Input.GetMouseButtonDown(0))
		{
			_mousePosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0);
			Debug.Log(string.Format("[{0},{1},{2}]", _mousePosition.x, _mousePosition.y, _mousePosition.z));
		}
  }
}

