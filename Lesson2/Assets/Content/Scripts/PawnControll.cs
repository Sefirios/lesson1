﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

[RequireComponent(typeof(SpriteRenderer))]
public class PawnControll : MonoBehaviour 
{
	[Range(50,100)]
	[SerializeField]
	private float _pawnSpeedMin;
	[Range(200,300)]
	[SerializeField]
	private float _pawnSpeedMax;
	private ApplicationContainer _root;

	private const float PI = 3.14f;
	private const int PIXEL_PER_UNIT = 1;
	private float _pawnSpeed;
	private SpriteSize _screenSize;
	private SpriteSize _spriteSize;
	private SpriteRenderer _spriteRenderer;
	private int _shiftPositionY;

	public ApplicationContainer Root
	{
		get { return _root; }
		set { _root=value; }
	}



	private void Start()
	{
		InitPawn();
		SetStartPosition();
	}

	private void Update()
	{
		PawnChangePosition();
	}

	private void InitPawn()
	{
		foreach(GameObject element in _root.AddedPrefabs)
		{
			Physics2D.IgnoreCollision(element.GetComponent<Collider2D>(), GetComponent<Collider2D>());
		}

		_spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
		_screenSize = new SpriteSize(Screen.width, Screen.height);
		if(_spriteRenderer.sprite != null)
		{
			_spriteSize = new SpriteSize(_spriteRenderer.sprite.texture.width/PIXEL_PER_UNIT, _spriteRenderer.sprite.texture.height/PIXEL_PER_UNIT);
		}
		else
		{
			_spriteSize = new SpriteSize();
		}

		_shiftPositionY = -_screenSize.Hight / 2 + _spriteSize.Hight / 2 + _spriteSize.Hight;
		_pawnSpeed = UnityEngine.Random.Range(_pawnSpeedMin, _pawnSpeedMax+1);
	}
	private void SetStartPosition()
	{
		transform.position = new Vector3(UnityEngine.Random.Range(-_screenSize.Widht / 2 + _spriteSize.Widht / 2, _screenSize.Widht / 2 - _spriteSize.Widht / 2+1), 
		                                 UnityEngine.Random.Range(-_screenSize.Hight / 2 + _spriteSize.Hight / 2, _shiftPositionY + 1));
	}

	private void PawnChangePosition()
	{
		transform.position = new Vector3(transform.position.x, transform.position.y + _pawnSpeed * Time.deltaTime);
	}
}

public class SpriteSize
{
	private int _widht;
	private int _hight;

	public SpriteSize()
	{
		_widht = 0;
		_hight = 0;
	}

	public SpriteSize(int widht, int hight)
	{
		_widht = widht;
		_hight = hight;
	}

	public int Widht
	{
		get { return _widht; }
	}
	public int Hight
	{
		get { return _hight; }
	}
}

